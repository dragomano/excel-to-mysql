<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <title>Excel to MySQL</title>
    </head>
    <body>
        <div class="container mt-3">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h1><a href="/">Excel to MySQL</a></h1>
                        </div>
                        <div class="card-body">
                            <?php if (!empty($_REQUEST['result']) && $_REQUEST['result'] == 'ok'): ?>
                            <div class="alert alert-success">
                                Данные добавлены в базу данных
                            </div>
                            <?php endif; ?>
                            <form action="upload.php" method="post" enctype="multipart/form-data">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" name="excel" accept="application/excel,application/vnd.ms-excel,application/x-excel,application/x-msexcel">
                                    <label class="custom-file-label" for="excel">Укажите файл для импорта</label>
                                </div>
                                <button class="btn btn-dark btn-block mt-1" type="submit">Отправить</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>