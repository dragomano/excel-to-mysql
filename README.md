# Excel To MySQL (PHP)

## Скрипт для перевода Excel-таблицы в базу данных MySQL

### Реализация
Название таблицы получается путем транслитерации имени файла, а заголовки столбцов — путем транслитерации заголовков на активном листе.
Все добавляемые столбцы имеют тип VARCHAR(255).

### Используемые компоненты:
* [Bootstrap](https://getbootstrap.com)
* [PhpSpreadsheet](https://github.com/PHPOffice/PhpSpreadsheet)