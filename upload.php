<?php

// Если не указан файл для загрузки, смысла в дальнейших действиях нет
if (empty($_FILES['excel']))
    die('Укажите рабочий файл!');

require 'vendor/autoload.php'; // Подключаем автозагрузчик композера

use Ausi\SlugGenerator\SlugGenerator; // Подключаем класс для генерации slug

$inputFile     = $_FILES['excel'];
$inputFileName = $inputFile['tmp_name'];
$fileInfo      = new \finfo(FILEINFO_MIME_TYPE); // Используем полезный класс для корректного определения mime-типа
$mimeType      = $fileInfo->file($inputFileName);

// Проверим соответствие mime-типа загруженного файла
if (in_array($mimeType, ['application/excel', 'application/vnd.ms-excel', 'application/x-excel', 'application/x-msexcel'])) {
    $spreadsheet        = \PhpOffice\PhpSpreadsheet\IOFactory::load($inputFileName);
    $worksheet          = $spreadsheet->getActiveSheet();
    $highestRow         = $worksheet->getHighestRow();
    $highestColumn      = $worksheet->getHighestColumn();
    $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);

    $columns = [];
    $rows    = [];

    $generator = new SlugGenerator; // Создадим класс генератора slug для транслитерации заголовков столбцов

    for ($row = 1; $row <= $highestRow; ++$row) {
        for ($col = 1; $col <= $highestColumnIndex; ++$col) {
            $value = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
            if ($row == 1) {
                $columns[] = $generator->generate($value);
            } else {
                $rows[$row - 2][] = $value;
            }
        }
    }

    $cn = \App\Db\Connection::make(); // Подключаемся к базе данных
    $db = new \App\Db\QueryBuilder($cn); // Подключаем класс для работы c таблицами

    // Теперь создадим таблицу по названию файла и добавим в нее соответствующие столбцы и данные
    $tableName = $generator->generate($inputFile['name']);

    if ($db->createTable($tableName, $columns)) {
        if ($db->insert($tableName, $rows) === false) {
            die('Ошибка при добавлении данных в таблицу!');
        } else {
            header('Location: /?result=ok');
        }
    } else {
        die('Ошибка при создании таблицы в базе данных!');
    }
} else {
    die('Формат файла не поддерживается!');
}
