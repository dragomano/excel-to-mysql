<?php

namespace App\Db;

class QueryBuilder
{
    private $db;

    public function __construct(\PDO $pdo)
    {
        $this->db = $pdo;
    }

    public function createTable(string $table, array $columns): bool
    {
        $columnsViaSemicolon = '';
        foreach ($columns as $column) {
            $columnsViaSemicolon .= ' `' . $column . '` varchar(255) NOT NULL,';
        }

        if (empty($columnsViaSemicolon))
            return false;

        $sql = 'CREATE TABLE IF NOT EXISTS `' . $table . '` (
            `id` int(11) NOT NULL AUTO_INCREMENT,' . $columnsViaSemicolon . '
            PRIMARY KEY (`id`)
            ) Engine=InnoDB DEFAULT CHARSET=utf8;';

        if ($this->db->query($sql)) {
            return true;
        }

        return false;
    }

    public function insert(string $table, array $rows): bool
    {
        $params = implode(",", array_fill(0, count($rows[0]), "?"));
        $tuples = "(NULL," . implode("),(NULL,", array_fill(0, count($rows), $params)) . ")";

        $sql = "INSERT INTO `$table` VALUES $tuples";

        $values = call_user_func_array("array_merge", $rows);
        $stmt   = $this->db->prepare($sql);

        return $stmt->execute($values);
    }
}