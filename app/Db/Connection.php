<?php

namespace App\Db;

class Connection
{
    public static function make()
    {
        $db = (require __DIR__ . '/../../settings.php');

        try {
            return new \PDO(
                'mysql:host=' . $db['host'] . ';dbname=' . $db['dbname'] . ';charset=utf8',
                $db['user'],
                $db['password']
            );
        } catch (\PDOException $e) {
            throw new DbException('Ошибка при подключении к базе данных: ' . $e->getMessage());
        }
    }
}